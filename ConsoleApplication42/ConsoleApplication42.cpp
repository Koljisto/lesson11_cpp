﻿#include <iostream>
#include <vector>
#include <string>
#include <algorithm>
#include <time.h>
#include <Windows.h>

using namespace std;
struct People {
	string specialty;
	char gender;
	int age;
	People(string specialty1,
		char gender1,
		int age1) :
		specialty(specialty1),
		gender(gender1),
		age(age1) {}
	People() {
		specialty = "None";
		gender = 'N';
		age = 0;
	}
};
People* auto_class_flat_cin(int n) {
	string specialty_people[10] = {
	 "Консультант по туризму", "Эндокринолог (специалист по гормонам)", "Промышленный дизайнер",
	 "Журналист", "Специалист по ВЭД", "Страховой агент",
	 "Месильщик", "Оценщик", "Повар", "Танцор балета" };
	char gender_list[10] = { 'M', 'F', 'M', 'F', 'M', 'F', 'M', 'F', 'M', 'F' };
	int age_array[10] = { 25, 36, 47, 58, 19, 67, 46, 45, 56, 34 };
	People* people = new People[n];
	random_shuffle(specialty_people, specialty_people + n);
	random_shuffle(gender_list, gender_list + n);
	random_shuffle(age_array, age_array + n);
	for (size_t i = 0; i < n; i++) {
		people[i] = People(specialty_people[i], gender_list[i], age_array[i]);
	}
	return people;
}
int main()
{
	srand(time(NULL));
	setlocale(LC_ALL, "Russian");
	SetConsoleCP(1251);
	SetConsoleOutputCP(1251);
	string specialty_people;
	char gender;
	int age;
	bool is_auto;
	int n;
	cout << "Введите количество квартир (если указали до 10, то можно использовать автоматическое заполнение): ";
	cin >> n; cout << endl;
	People* Peoples = new People[n];
	cout << "Использовать автоматический ввод? 0 - Нет, 1 - Да: ";
	cin >> is_auto; cout << endl;
	if (is_auto && n <= 10) {
		cout << "Автоматический ввод..." << endl;
		Peoples = auto_class_flat_cin(n);
	}
	else {
		cout << "Ручной ввод..." << endl;
		for (size_t i = 0; i < n; i++) {
			cout << "Введите специальность: ";
			cin >> specialty_people;
			cout << "Введите гендер: ";
			cin >> gender; cout << endl;
			cout << "Введите возраст: ";
			cin >> age; cout << endl;
			People temp = People(specialty_people, gender, age);
			Peoples[i] = temp;
		}
	}
	for (size_t i = 0; i < n; i++) {
		if (Peoples[i].gender == 'M') {
			cout << "Специальность: " << Peoples[i].specialty << ", ";
			cout << "Гендер: " << Peoples[i].gender << ", ";
			cout << "Возраст: " << Peoples[i].age << ". " << endl;
		}
	}
	delete[] Peoples;
}